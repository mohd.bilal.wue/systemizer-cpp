/*
 * Block.cpp
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#include "Block.h"
#include "OutputPort.h"
#include "InputPort.h"

Block::Block() {
	name = "untitled";
	parent = NULL;
	children = {};
	out = new OutputPort(name + "_out");
	in = new InputPort(name + "_out");
}

Block::Block(string strName) {
	name = strName;
	parent = NULL;
	children = {};
	out = new OutputPort(name + "_out");
	in = new InputPort(name + "_out");
}

void Block::connect(IConnector * connector, string port) {
	if (port.compare("O")==0) {
		connectOut(connector);
	} else if (port.compare("I")==0) {
		connectIn(connector);
	}

}
vector<IBlock *> Block::getChildren() {
	return children;
}
IBlock * Block::getChild(string name) {
	if (children.empty()) {
		return NULL;
	} else {
		for (IBlock * child : children) {
			if (child->getName().compare(name) == 0) {
				return child;
			}
		}
	}
	return NULL;
}

IBlock * Block::getParent() {
	return parent;
}
void Block::setParent(IBlock * block) {
	parent = block;
}
string Block::getName() {
	return name;
}
void Block::addBlock(IBlock * block) {
	block->setParent(this);
	children.push_back(block);
}

IPort * Block::getOutPort() {
	return out;
}
IPort * Block::getInPort() {
	return in;
}

void Block::send(){
	out->send();
}


void Block::connectIn(IConnector * connector){
	in->bind(connector);
}
void Block::connectOut(IConnector * connector){
	out->bind(connector);
}

Block::~Block() {
	// TODO Auto-generated destructor stub
}

