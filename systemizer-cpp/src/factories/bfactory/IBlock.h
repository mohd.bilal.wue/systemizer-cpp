/*
 * IBlock.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_BFACTORY_IBLOCK_H_
#define FACTORIES_BFACTORY_IBLOCK_H_
#pragma once
#include <string>
#include <iostream>
#include <vector>

using namespace std;
class IConnector;
class IPort;

class IBlock {
public:
	virtual ~IBlock() {
	}

	virtual void connect(IConnector * connector, string port)=0;

	virtual vector<IBlock *> getChildren()=0;

	virtual IBlock * getChild(string name)=0;

	virtual IBlock * getParent()=0;

	virtual void setParent(IBlock * block)=0;

	virtual string getName()=0;

	virtual void addBlock(IBlock * block)=0;

	virtual IPort * getOutPort()=0;

	virtual IPort * getInPort()=0;

};

#endif /* FACTORIES_BFACTORY_IBLOCK_H_ */
