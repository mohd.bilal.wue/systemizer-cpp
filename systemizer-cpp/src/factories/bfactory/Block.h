/*
 * Block.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_BFACTORY_BLOCK_H_
#define FACTORIES_BFACTORY_BLOCK_H_

#pragma once
#include <string>
#include "IBlock.h"

using namespace std;

class Block: public IBlock {
public:
	Block();
	Block(string name);
	virtual ~Block();
	void connect(IConnector * connector, string port);
	vector<IBlock *> getChildren();
	IBlock * getChild(string name);
	IBlock * getParent();
	void setParent(IBlock * block);
	string getName();
	void addBlock(IBlock * block);
	IPort * getOutPort();
	IPort * getInPort();
	void send();


private:
	vector<IBlock *> children;
	string name;
	IBlock * parent;
	IPort * out;
	IPort * in;
	void connectIn(IConnector * connector);
	void connectOut(IConnector * connector);
};

#endif /* FACTORIES_BFACTORY_BLOCK_H_ */
