/*
 * InputPort.cpp
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#include <iostream>
#include "InputPort.h"

InputPort::InputPort() :
		Port() {
}

InputPort::InputPort(string name) :
		Port(name) {
}

InputPort::~InputPort() {
}

void InputPort::receive() {
	for (IConnector * iConnector : connectors) {
		cout << "Receiving data from connector!";
	}
}

