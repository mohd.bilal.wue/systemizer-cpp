/*
 * InputPort.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_PORTFACTORY_INPUTPORT_H_
#define FACTORIES_PORTFACTORY_INPUTPORT_H_
#pragma once

#include "Port.h"

class InputPort: public Port {
public:
	InputPort();
	InputPort(string name);
	virtual ~InputPort();
	void receive();
};

#endif /* FACTORIES_PORTFACTORY_INPUTPORT_H_ */
