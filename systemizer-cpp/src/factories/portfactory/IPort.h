/*
 * IPort.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_PORTFACTORY_IPORT_H_
#define FACTORIES_PORTFACTORY_IPORT_H_
#pragma once

class IConnector;

class IPort {
public:
	virtual ~IPort() {
	}
	virtual void send()=0 ;
	virtual void receive()=0;
	virtual void bind(IConnector * connector)=0;
};

#endif /* FACTORIES_PORTFACTORY_IPORT_H_ */
