/*
 * OutputPort.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_PORTFACTORY_OUTPUTPORT_H_
#define FACTORIES_PORTFACTORY_OUTPUTPORT_H_
#pragma once
#include "Port.h"

class OutputPort: public Port {
public:
	OutputPort();
	OutputPort(string name);
	virtual ~OutputPort();

};

#endif /* FACTORIES_PORTFACTORY_OUTPUTPORT_H_ */
