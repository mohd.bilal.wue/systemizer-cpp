/*
 * Port.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_PORTFACTORY_PORT_H_
#define FACTORIES_PORTFACTORY_PORT_H_
#pragma once

#include <vector>
#include <string>
#include "IPort.h"

using namespace std;


class Port: public IPort {
public:

	Port();
	Port(string name);
	virtual ~Port();
	void send();
	void receive();
	void bind(IConnector * connector);
	string getName();
	vector<IConnector *> getConnectors();

private:
	string name;

protected:
	vector<IConnector *> connectors;
};

#endif /* FACTORIES_PORTFACTORY_PORT_H_ */
