/*
 * Port.cpp
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#include "Port.h"
#include "IConnector.h"
Port::Port() {
	name = "untitled";
	connectors = {};
}

Port::Port(string name) {
	this->name = name;
	connectors = {};
}

Port::~Port() {
	// TODO Auto-generated destructor stub
}


void Port::send() {
	for (IConnector * iConnector: connectors) {
			iConnector->transfer();
	}
}

void Port::bind(IConnector * connector) {
	connectors.push_back(connector);
}

void Port::receive() {

}

string Port::getName() {
	return name;
}

vector<IConnector *> Port::getConnectors() {
	return connectors;
}

