/*
 * Connector.cpp
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */
#include <iostream>
#include "Connector.h"
#include "IBlock.h"

Connector::Connector() {
	name = "untitled";
	headBlock = NULL;
	tailBlock = NULL;
}

Connector::Connector(string name) {
	this->name = name;
	headBlock = NULL;
	tailBlock = NULL;
}

Connector::~Connector() {
	// TODO Auto-generated destructor stub
}

void Connector::transfer() {
	cout
			<< "Sending data downstream to block " + headBlock->getName()
					+ " via " + name + "!";
}
void Connector::connectTail(IBlock * driver, string side) {
	tailBlock = driver;
	tailBlock->connect(this, side);
}

void Connector::connectHead(IBlock * driven, string side) {
	headBlock = driven;
	headBlock->connect(this, side);
}

IBlock * Connector::getHead() {
	return headBlock;
}

IBlock * Connector::getTail() {
	return tailBlock;
}
