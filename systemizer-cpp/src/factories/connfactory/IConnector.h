/*
 * IConnector.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_CONNFACTORY_ICONNECTOR_H_
#define FACTORIES_CONNFACTORY_ICONNECTOR_H_
#pragma once

#include <string>

using namespace std;

class IBlock;

class IConnector {
public:
	virtual ~IConnector(){};
	virtual void transfer()=0;
	virtual IBlock * getHead()=0;
	virtual IBlock * getTail()=0;
	virtual void connectTail(IBlock * driver, string side)=0;
	virtual void connectHead(IBlock * driven, string side)=0;
};

#endif /* FACTORIES_CONNFACTORY_ICONNECTOR_H_ */
