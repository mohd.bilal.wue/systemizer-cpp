/*
 * Connector.h
 *
 *  Created on: 15 Mar 2020
 *      Author: Bilal
 */

#ifndef FACTORIES_CONNFACTORY_CONNECTOR_H_
#define FACTORIES_CONNFACTORY_CONNECTOR_H_
#pragma once
#include "IConnector.h"

class Connector: public IConnector {
public:
	Connector();
	Connector(string name);
	virtual ~Connector();
	void transfer();
	IBlock * getHead();
	IBlock * getTail();
	void connectTail(IBlock * driver, string side);
	void connectHead(IBlock * driven, string side);


private:
	string name;
	IBlock * headBlock;
	IBlock * tailBlock;
};

#endif /* FACTORIES_CONNFACTORY_CONNECTOR_H_ */
