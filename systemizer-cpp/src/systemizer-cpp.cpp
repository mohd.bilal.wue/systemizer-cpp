//============================================================================
// Name        : systemizer-cpp.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "Connector.h"
#include "Block.h"

using namespace std;

int main() {
	Connector conn{"ADCS-EPS"};
	Block b1{"ADCS"};
	Block b2{"EPS"};
	conn.connectHead(&b2, "I");
	conn.connectTail(&b1, "O");
	b1.send();
	return 0;
}
